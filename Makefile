ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

-include pre_local.mk

init-dev:
	pipenv install --dev

db-migrate:
	PYTHONPATH=. alembic upgrade head

db-add:
	PYTHONPATH=. alembic revision --autogenerate

db-undo:
	PYTHONPATH=. alembic downgrade -1

run-api:
	PYTHONPATH=. python -m moviedb.app

run-import:
	python -m tools.import_data $(ROOT_DIR)/data/movies.json

run-check-actors:
	python -m tools.import_data --check-actors $(ROOT_DIR)/data/movies.json

test:
	pytest --disable-pytest-warnings --cov=moviedb tests

check:
	pylint moviedb && mypy moviedb

-include post_local.mk
