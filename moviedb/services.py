from typing import Optional, List

from sqlalchemy import func
from sqlalchemy.exc import IntegrityError

from moviedb.models.db import current_db_session as session, db_get_or_create as get_or_create
from moviedb.models.models import Movie, Genre, Actor, ActorMovie


class MoviesService:

    @classmethod
    def resolve_genres(cls, names: List[str]) -> List[Genre]:
        return session.query(Genre).filter(Genre.name.in_(names)).all() if names else []

    @classmethod
    def resolve_actors(cls, names: List[str]) -> List[Actor]:
        return session.query(Actor).filter(Actor.name.in_(names)).all() if names else []

    @classmethod
    def create_movie(cls, title: str, year: int,
                     genres: List[str] = None,
                     actors: List[str] = None) -> Movie:
        movie, exists_movie = get_or_create(session, Movie, title=title, year=year)
        if exists_movie:
            raise ValueError('movie exists')

        if genres:
            movie.genres = cls.resolve_genres(genres)
        if actors:
            movie.actors = cls.resolve_actors(actors)
        session.commit()
        return movie

    @classmethod
    def change_movie(cls, movie_id: int,
                     title: str = None,
                     year: int = None,
                     genres: List[str] = None,
                     actors: List[str] = None) -> Movie:
        movie = cls.get_movie(movie_id)
        if not movie:
            raise ValueError('movie not exists')

        try:
            params = (('title', title), ('year', year))
            params = dict(x for x in params if x[1])
            if params:
                session.query(Movie).filter(Movie.id == movie_id).update(params)

            if genres is not None:
                movie.genres = cls.resolve_genres(genres)
            if actors is not None:
                movie.actors = cls.resolve_actors(actors)
            session.commit()

        except IntegrityError:
            raise ValueError('movie title and year exists')

        return movie

    @classmethod
    def get_movie(cls, movie_id: int) -> Optional[Movie]:
        return session.query(Movie).get(movie_id)

    @classmethod
    def delete_movie(cls, movie_id: int) -> bool:
        deleted = session.query(Movie).filter(Movie.id == movie_id).delete()
        session.commit()
        return deleted > 0

    @classmethod
    def search(cls, query: str):
        if not query:
            return []

        q = session.query(Actor).join(ActorMovie).join(Movie)
        q = q.with_entities(
            Actor.name.label('actor'),
            Movie.year.label('year'),
            func.count('*').label('movies_count')
        )
        q = q.group_by('actor', 'year')
        q = q.order_by('actor', 'year')
        q = q.filter(Actor.name.like(f'%{query}%'))

        return q.all()


movies_service = MoviesService()
