from functools import partial
from pathlib import Path
from typing import Tuple, Any

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

ROOT_PATH = Path(__file__).parent.parent.parent.absolute()
DB_PATH = 'sqlite:///' + str(ROOT_PATH / 'data' / 'movies.db')

__session_cls = sessionmaker(bind=create_engine(DB_PATH, echo=True))
current_db_session = __session_cls()


def db_get_or_create(session, model, commit=False, **kwargs) -> Tuple[Any, bool]:  # obj, exists
    # TODO: Use SQLite upsert?
    ret = session.query(model).filter_by(**kwargs).first()
    if ret:
        return ret, True

    ret = model(**kwargs)
    session.add(ret)
    if commit:
        session.commit()
    return ret, False


def make_db_get_or_create(session):
    return partial(db_get_or_create, session)
