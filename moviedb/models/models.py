from sqlalchemy import Column, ForeignKey, Index, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from .base import Base

metadata = Base.metadata  # type: ignore


class Movie(Base):
    __tablename__ = 'movie'

    id = Column(Integer, primary_key=True)
    title = Column(String(1))
    year = Column(Integer)

    @classmethod
    def init_relationship(cls):
        cls.genres = relationship('Genre', secondary='genre_movie', lazy='dynamic')
        cls.actors = relationship('Actor', secondary='actor_movie', lazy='dynamic')

    __table_args__ = (
        Index('ix_movie_title_year', title, year, unique=True),
    )


class Actor(Base):
    __tablename__ = 'actor'

    id = Column(Integer, primary_key=True)
    name = Column(String(1), index=True, unique=True)

    def __str__(self):
        return self.name


class ActorMovie(Base):
    __tablename__ = 'actor_movie'

    id = Column(Integer, primary_key=True)
    actor_id = Column(ForeignKey('actor.id', ondelete='CASCADE'))
    movie_id = Column(ForeignKey('movie.id', ondelete='CASCADE'))

    __table_args__ = (
        UniqueConstraint(actor_id, movie_id, sqlite_on_conflict='IGNORE'),
    )


class Genre(Base):
    __tablename__ = 'genre'

    id = Column(Integer, primary_key=True)
    name = Column(String(1), index=True, unique=True)

    def __str__(self):
        return self.name


class GenreMovie(Base):
    __tablename__ = 'genre_movie'

    id = Column(Integer, primary_key=True)
    genre_id = Column(ForeignKey('genre.id', ondelete='CASCADE'))
    movie_id = Column(ForeignKey('movie.id', ondelete='CASCADE'))

    __table_args__ = (
        UniqueConstraint(genre_id, movie_id, sqlite_on_conflict='IGNORE'),
    )


def init_relationship():
    Movie.init_relationship()
