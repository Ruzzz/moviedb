from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from moviedb.api_v1.schemas import MovieInOutSchema, MovieExtOutSchema, MoviePatchInSchema, SearchOutSchema
from moviedb.core.utils import to_bool
from moviedb.services import movies_service

api_v1 = Blueprint('v1', __name__)


movie_schema = MovieInOutSchema()
movie_ext_schema = MovieExtOutSchema()
movie_patch_schema = MoviePatchInSchema()
search_schema = SearchOutSchema(many=True)


@api_v1.route('/movie/<int:movie_id>', methods=['GET'])
def get_movie(movie_id):
    movie = movies_service.get_movie(movie_id)
    if not movie:
        return {'status': 'error', 'msg': 'movie not found'}, 404

    schema = movie_schema if not to_bool(request.args.get('ext')) else movie_ext_schema
    return schema.dump(movie)


@api_v1.route('/movie/<int:movie_id>', methods=['PATCH'])
def change_movie(movie_id):
    data = request.get_json()
    if not data:
        return {'status': 'error', 'msg': 'no movie data'}, 400

    try:
        data = movie_patch_schema.load(data)
    except ValidationError as err:
        return {'statuses': err.messages}, 422

    try:
        movie = movies_service.change_movie(movie_id, **data)
    except ValueError as err:  # TODO: Use app defined exceptions
        return {'status': 'error', 'msg': str(err)}, 422

    schema = movie_schema if not to_bool(request.args.get('ext')) else movie_ext_schema
    return schema.dump(movie)


@api_v1.route('/movie', methods=['POST'])
def create_movie():
    data = request.get_json()
    if not data:
        return {'status': 'error', 'msg': 'no movie data'}, 400

    try:
        data = movie_schema.load(data)
    except ValidationError as err:
        return {'statuses': err.messages}, 422

    try:
        movie = movies_service.create_movie(**data)
    except ValueError as err:  # TODO: Use app defined exceptions
        return {'status': 'error', 'msg': str(err)}, 422

    schema = movie_schema if not to_bool(request.args.get('ext')) else movie_ext_schema
    return schema.dump(movie)


@api_v1.route('/movie/<int:movie_id>', methods=['DELETE'])
def delete_movie(movie_id):
    if not movies_service.delete_movie(movie_id):
        return {'status': 'error', 'msg': 'movie not found'}, 404
    return {'status': 'ok'}


@api_v1.route('/search', methods=['GET'])
def search():
    ret = movies_service.search(request.args.get('q'))
    return jsonify(search_schema.dump(ret))
