from marshmallow import Schema, fields
from marshmallow.validate import Range, Length

FIELD_ID = fields.Int(required=True, validate=Range(min=1))


class MovieInOutSchema(Schema):
    title = fields.Str(required=True, validate=Length(min=1, max=80))
    year = fields.Int(required=True, validate=Range(min=1, max=3000))
    genres = fields.List(fields.Str)
    actors = fields.List(fields.Str)


class MoviePatchInSchema(Schema):
    title = fields.Str(validate=Length(min=1, max=80))
    year = fields.Int(validate=Range(min=1, max=3000))
    genres = fields.List(fields.Str)
    actors = fields.List(fields.Str)


class _NameSchema(Schema):
    id = FIELD_ID
    name = fields.Str(required=True, validate=Length(min=1, max=80))


class MovieExtOutSchema(MovieInOutSchema):
    id = FIELD_ID
    genres = fields.Nested(_NameSchema, many=True)
    actors = fields.Nested(_NameSchema, many=True)


class SearchOutSchema(Schema):
    actor = fields.Str()
    year = fields.Int()
    movies_count = fields.Int()
