import re
import string
import unicodedata
from typing import Optional


class PersonNameNormalizer:
    _TK_REGEXS = {
        r'[A-Za-z]+\.?',
        r'[A-Z]\.[A-Z]\.',  # D.J. ...
        '[A-Za-z]+[A-Za-z\'-]+',  # Erin O'Brien-Moore
    }
    _TK_RE = re.compile('|'.join(f'(?:{x})' for x in _TK_REGEXS))
    _TK_ARTICLES = {
        'de',
        'del',
        'la',
        'the',
        'von',
    }
    _TK_PREFIXES = (
        'd\'',
    )
    _NAME_REMOVES = [
        ('', re.compile(r'\s+\(\w+$', flags=re.MULTILINE)),  # (WORD) on end
        (' ', re.compile(r'(?<=[A-Za-z])\s+[\"\'].+[\"\']\s+(?=[A-Za-z])')),  # nick, ex: Jeff "Ja Rule" Atkins
        (' ', re.compile(r'\s+'))
    ]
    _NAME_ONE_TOKENS = {  # TODO: Review it
        'Cher',
        'Rihanna',
    }

    def __call__(self, name: str) -> Optional[str]:
        name = name.strip(' ()[]')

        # hardcode
        name = name.replace(', Jr.', ' Jr.')

        for repl, regex in self._NAME_REMOVES:
            name = regex.sub(repl, name)
            if not name:
                return None

        if not name:
            return None
        tokens = name.split(' ')
        total = len(tokens)
        if not total:
            return None

        ret = []
        for token in tokens:
            # Ex.: Acuña -> Acuna
            token = unicodedata.normalize('NFKD', token).encode('ascii', 'ignore').decode('ascii')
            if not (token and self._valid_token(token, total)):
                return None
            ret.append(token)

        if len(ret) == 1 and ret[0] not in self._NAME_ONE_TOKENS:
            return None

        return ' '.join(ret)

    @classmethod
    def _valid_token(cls, token: str, total: int) -> bool:
        if len(token) == 1 and token[0] in string.ascii_lowercase + string.digits:
            return False

        # TODO: Maybe later, for now we just delete all nicknames
        # Unquote, Ex.: Adam "Edge" Copeland
        # if (token[0] == token[-1] == '"') or (token[0] == token[-1] == '\''):
        #     if total == 1:
        #         return False
        #     token = token[1:-1]
        #     if not token:
        #         return False

        token = cls._try_remove_prefix(token)
        return bool(
            token in cls._TK_ARTICLES or
            cls._TK_RE.fullmatch(token) or
            (token.isdigit() and total > 1)  # Ex.: 50 Cent
        )

    @classmethod
    def _try_remove_prefix(cls, token: str) -> str:
        for prefix in cls._TK_PREFIXES:
            if token.startswith(prefix):
                return token[len(prefix):]
        return token
