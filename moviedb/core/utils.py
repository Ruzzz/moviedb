
def to_bool(value):
    return value in {'1', 'True', 'true', True}
