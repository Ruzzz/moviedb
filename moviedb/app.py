from flask import Flask

from moviedb.models.models import init_relationship
from .api_v1.resources import api_v1


def main():
    app = Flask('MoviesDB API')
    app.register_blueprint(api_v1)
    init_relationship()
    app.run()


if __name__ == '__main__':
    main()
