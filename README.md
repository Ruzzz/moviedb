## About

Example of simple REST API based on:

- flask
- sqlite + sqlalchemy

## Development Environment

```
make init-dev
make db-migrate
```

## Run

```
make run-check-actors
make run-import (long op, about 1-2 minutes)
make run-api
```

## Development

```
make test
make check
```

## API

```
GET    http://127.0.0.1:5000/movie/1
GET    http://127.0.0.1:5000/movie/1?ext=1
DELETE http://127.0.0.1:5000/movie/1
PATCH  http://127.0.0.1:5000/movie/1 - Change movie
POST   http://127.0.0.1:5000/movie   - Create movie
```

In data example:

```json
{
    "title": "New Cool Film",
    "year": 2000,
    "actors": ["Claire Whitney"],
    "genres": ["Western", "Biography"]
}
```

```
GET http://127.0.0.1:5000/search?q=Clark - Search actors and getting aggregated data
```

Out data example:

```json
[
    {
        "actor": "Wallace Clarke",
        "movies_count": 1,
        "year": 1916
    },
    {
        "actor": "William Clark",
        "movies_count": 1,
        "year": 2013
    }
]
```