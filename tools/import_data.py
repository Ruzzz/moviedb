import argparse
import json
import warnings
from collections import defaultdict
from functools import lru_cache

import sqlalchemy.exc

from moviedb.core.persons import PersonNameNormalizer
from moviedb.models.db import current_db_session, make_db_get_or_create
from moviedb.models.models import Actor, ActorMovie, Genre, GenreMovie, Movie

normalize_person_name = PersonNameNormalizer()


def import_data(session, data: list):
    get_or_create = make_db_get_or_create(session)

    @lru_cache(maxsize=2000)
    def get_actor(name):
        ret_, _ = get_or_create(Actor, session, name=name)
        return ret_

    @lru_cache(maxsize=200)
    def get_genre(name):
        ret_, _ = get_or_create(Genre, session, name=name)
        return ret_

    mid_actors_map = defaultdict(list)
    mid_genres_map = defaultdict(list)

    def update_():
        session.commit()

        with warnings.catch_warnings():
            warnings.simplefilter('ignore', category=sqlalchemy.exc.SAWarning)

            nonlocal mid_actors_map
            for movie, actors in mid_actors_map.items():
                for actor in actors:
                    session.add(ActorMovie(movie_id=movie.id, actor_id=actor.id))
            mid_actors_map = defaultdict(list)

            nonlocal mid_genres_map
            for movie, genres in mid_genres_map.items():
                for genre in genres:
                    session.add(GenreMovie(movie_id=movie.id, genre_id=genre.id))
            mid_genres_map = defaultdict(list)
            session.commit()

    total_items = len(data)
    for item_no, movie_info in enumerate(data):
        movie, exists_movie = get_or_create(
            Movie,
            session,
            title=movie_info['title'],
            year=int(movie_info['year']))
        # if exists_movie:
        #     print('Duplicated movie:', movie.title, f'({movie.year})')

        for actor_name in movie_info.get('cast', []):
            normalized_actor_name = normalize_person_name(actor_name)
            if not normalized_actor_name:
                # print('Skip invalid actor name:', actor_name)
                continue
            mid_actors_map[movie].append(get_actor(normalized_actor_name))

        for genre_name in movie_info.get('genres', []):
            mid_genres_map[movie].append(get_genre(genre_name))

        if not (item_no % 1000):
            update_()
            print('Imported:', f'{item_no}/{total_items} ({int(item_no / total_items * 100)}%)')

    update_()


def check_actors(data: list):
    for movie_info in data:
        for actor_name in movie_info.get('cast', []):
            normalized_actor_name = normalize_person_name(actor_name)
            if not normalized_actor_name:
                print(actor_name)


def main():
    parser = argparse.ArgumentParser(description='Movies JSON data importer')
    parser.add_argument('file', nargs='+', type=argparse.FileType('r'), help='JSON data filename.')
    parser.add_argument('--check-actors', action='store_true')
    args = parser.parse_args()

    for fp in args.file:
        try:
            data = json.load(fp)
            fp.close()
        except Exception:
            print('Open file failed:', fp.name)
            continue

        if args.check_actors:
            check_actors(data)
        else:
            import_data(current_db_session, data)


if __name__ == '__main__':
    main()
